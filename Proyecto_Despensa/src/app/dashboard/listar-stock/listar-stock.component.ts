import { Component, OnInit, ViewChild } from '@angular/core';
import { FirestoreResponse } from 'src/app/models/firestore-response.interface';
import { Stock } from 'src/app/models/stock.interface';
import { MatSnackBar, MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { UsersService } from 'src/app/services/users.service';
import { StockNewDialogComponent } from '../stock-new-dialog/stock-new-dialog.component';

@Component({
  selector: 'app-listar-stock',
  templateUrl: './listar-stock.component.html',
  styleUrls: ['./listar-stock.component.scss']
})
export class ListarStockComponent implements OnInit {

  
  listadoStock : FirestoreResponse<Stock>[];
  displayedColumns: string[] = ['producto', 'categoria','en_despensa','minimo','acciones'];
  dataSource : MatTableDataSource<FirestoreResponse<Stock>>

  @ViewChild(MatPaginator,{static:true}) paginator : MatPaginator;
  
  constructor(
    private stockService: UsersService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadStock();
  }

  loadStock() {
    this.stockService.getStock().subscribe(resp=>{
      this.listadoStock = [];

      resp.forEach((stock :any) => {
          this.listadoStock.push({
            id : stock.payload.doc.id, 
            data: stock.payload.doc.data() as Stock
          });
      });

      this.dataSource = new MatTableDataSource<FirestoreResponse<Stock>>(this.listadoStock)
      this.dataSource.paginator = this.paginator;

      
    });
  }

  dialogEditStock(producto : string, categoria: string, id : string,en_despensa : number, minimo :number){
    let dialogRef = this.dialog.open(StockNewDialogComponent,{
      data: { producto,categoria,id,nuevo:false,en_despensa,minimo}
    });
    dialogRef.afterClosed().subscribe(res => {
      if(res!=null){
        if(res){
          this.snackBar.open("Añadido correctamente")
        }
        else{
          this.snackBar.open("Error añadir producto")
        }
      }
    });
  }

  borrarStock(id : string){
    this.stockService.borrarStock(id);
  }

  confirmar(producto: string,id:string) {
    if(confirm("¿Está seguro de que desea eliminar el stock " + producto + "?")) {
      this.borrarStock(id.toString());
    }
  }

  modificarStock(cantidad : number,id :string){
    this.stockService.modificarStock(cantidad,id);
  }

}
