import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { ListarProductosComponent } from './listar-productos/listar-productos.component';
import { ListarCategoriasComponent } from './listar-categorias/listar-categorias.component';
import { ListarStockComponent } from './listar-stock/listar-stock.component';

export const DashboardRoutes: Routes = [
  { path: '', redirectTo: '../session/signin'},
  { path: 'stock', component: ListarStockComponent},
  { path: 'productos', component: ListarProductosComponent},
  { path: 'categorias', component: ListarCategoriasComponent},
  
  
];
