import { Component, OnInit } from '@angular/core';
import { Categoria } from 'src/app/models/categoria.interface';
import { FirestoreResponse } from 'src/app/models/firestore-response.interface';
import { CategoriasService } from 'src/app/services/categorias.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { CategoriaNewDialogComponent } from '../categoria-new-dialog/categoria-new-dialog.component';

@Component({
  selector: 'app-listar-categorias',
  templateUrl: './listar-categorias.component.html',
  styleUrls: ['./listar-categorias.component.scss']
})
export class ListarCategoriasComponent implements OnInit {

  listadoStories: FirestoreResponse<Categoria>[];
  displayedColumns: string[] = ['nombre','acciones'];

  constructor(
    private categoriasService: CategoriasService, 
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.loadCategorias();

  }

  
  loadCategorias() {
    this.categoriasService.getCategorias().subscribe(resp => {
      this.listadoStories = [];

      resp.forEach((story: any) => {
        this.listadoStories.push({ 
          id: story.payload.doc.id, 
          data: story.payload.doc.data() as Categoria 
        });
      });
      
    });
  }

  dialogNuevaCategoria() {
    let dialogRef = this.dialog.open(CategoriaNewDialogComponent, {
      width: '300px'
    });

    dialogRef.afterClosed().subscribe(resp => {
      if(resp != null) {
        if(resp) {
          this.snackBar.open("Historia creado correctamente", );
        } else {
          this.snackBar.open("Error al crear usuario");
        }
      }
    });
  }

  dialogEditCategoria(categoriaToEdit: Categoria, idCategoria:string) {
    let dialogRef = this.dialog.open(CategoriaNewDialogComponent, {
      width: '300px',
      data: { categoria: categoriaToEdit}
    });
  }

  deleteCategoria(idCategoria: string){
    this.categoriasService.deleteCategorias(idCategoria);
  }

}
