import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatProgressBarModule,
  MatTableModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatChipsModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatPaginatorModule
} from '@angular/material';

import { ChartsModule } from 'ng2-charts';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { FormsModule } from '@angular/forms';
import { ProductosService } from 'src/app/services/productos.service';
import {ProductoNewDialogComponent} from './producto-new-dialog/producto-new-dialog.component'
import {ListarProductosComponent} from './listar-productos/listar-productos.component';
import { ListarStockComponent } from './listar-stock/listar-stock.component';
import { ListarCategoriasComponent } from './listar-categorias/listar-categorias.component';
import { CategoriaNewDialogComponent } from './categoria-new-dialog/categoria-new-dialog.component';
import { CategoriasService } from '../services/categorias.service';
import { AuthService } from '../services/auth.service';
import { StockNewDialogComponent } from './stock-new-dialog/stock-new-dialog.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardRoutes),
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    MatMenuModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatChipsModule,
    ChartsModule,
    FormsModule,
    NgxDatatableModule,
    FlexLayoutModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  declarations: [DashboardComponent, ListarProductosComponent, ProductoNewDialogComponent, ListarStockComponent,ListarCategoriasComponent, CategoriaNewDialogComponent,ListarStockComponent, StockNewDialogComponent],
  entryComponents: [ProductoNewDialogComponent,CategoriaNewDialogComponent,StockNewDialogComponent],
  providers: [
    ProductosService,
    CategoriasService,
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2000}},
    AuthService
  ]
})
export class DashboardModule {}
