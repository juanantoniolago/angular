import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/productos.interface';
import { FirestoreResponse } from 'src/app/models/firestore-response.interface';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ProductosService } from 'src/app/services/productos.service';
import { ProductoNewDialogComponent } from '../producto-new-dialog/producto-new-dialog.component';
import { StockNewDialogComponent } from '../stock-new-dialog/stock-new-dialog.component';

@Component({
  selector: 'app-listar-productos',
  templateUrl: './listar-productos.component.html',
  styleUrls: ['./listar-productos.component.scss']
})
export class ListarProductosComponent implements OnInit {

  listadoProductos: FirestoreResponse<Producto>[];
  displayedColumns: string[] = ['nombre', 'descripcion','acciones'];

  constructor(
    private productosService: ProductosService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadProductos();
  }

  loadProductos() {
    this.productosService.getProductos().subscribe(resp => {
      this.listadoProductos = [];

      resp.forEach((producto: any) => {
        this.listadoProductos.push({ 
          id: producto.payload.doc.id, 
          data: producto.payload.doc.data() as Producto 
        });
      });
      
    });
  }

  dialogNuevoProducto() {
    let dialogRef = this.dialog.open(ProductoNewDialogComponent, {
      width: '300px'
    });

    dialogRef.afterClosed().subscribe(resp => {
      if(resp != null) {
        if(resp) {
          this.snackBar.open("Historia creado correctamente", );
        } else {
          this.snackBar.open("Error al crear usuario");
        }
      }
    });
  }

  deleteProducto(idProducto: string){
    this.productosService.deleteProductos(idProducto);
  }

  dialogNuevoStock(producto : string, categoria: string, id : string){
    let dialogRef = this.dialog.open(StockNewDialogComponent,{
      data: { producto,categoria,id,nuevo:true}
    });
    dialogRef.afterClosed().subscribe(res => {
      if(res!=null){
        if(res){
          this.snackBar.open("Añadido correctamente")
        }
        else{
          this.snackBar.open("Error añadir producto")
        }
      }
    });
  }


  

}
