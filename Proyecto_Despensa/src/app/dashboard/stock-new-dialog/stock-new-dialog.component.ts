import { Component, OnInit, Inject } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StockDto } from 'src/app/models/stock.dto';

export interface DatosEntradaDialog {
  producto: string;
  categoria : string;
  id : string;
  nuevo :boolean;
  en_despensa :number;
  minimo :number;
}

@Component({
  selector: 'app-stock-new-dialog',
  templateUrl: './stock-new-dialog.component.html',
  styleUrls: ['./stock-new-dialog.component.scss']
})
export class StockNewDialogComponent implements OnInit {
  
  stockDto: StockDto;

  constructor(public dialogRef : MatDialogRef<StockNewDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DatosEntradaDialog,
    private userService : UsersService) { }

  ngOnInit() {
    this.stockDto = new StockDto(this.data.producto,this.data.categoria,this.data.en_despensa,this.data.minimo)

  }

  cerrar() {
    this.dialogRef.close(null);
  }

  guardarStock(){
    this.userService.nuevoStock(this.stockDto,this.data.id);
    this.dialogRef.close();
  }

  
 

 

}
