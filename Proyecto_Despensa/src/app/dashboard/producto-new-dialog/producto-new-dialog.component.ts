import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import { ProductoDto } from 'src/app/models/productos.dto';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-producto-new-dialog',
  templateUrl: './producto-new-dialog.component.html',
  styleUrls: ['./producto-new-dialog.component.scss']
})
export class ProductoNewDialogComponent implements OnInit {
  
  productoDto: ProductoDto;

  constructor(public dialogRef: MatDialogRef<ProductoNewDialogComponent>,
    private productosService: ProductosService) { }

  ngOnInit() {
    this.productoDto = new ProductoDto('', '','');

  }

  cerrar() {
    this.dialogRef.close(null);
  }

  guardarProducto() {
    this.productosService.createProducto(this.productoDto).then(respCorrecta => {
      this.dialogRef.close(true);
    }).catch(respError => {
      this.dialogRef.close(false);
    });
  }

}
