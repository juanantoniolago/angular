import { Component, OnInit } from '@angular/core';
import { CategoriaDto } from 'src/app/models/categoria.dto';
import { MatDialogRef } from '@angular/material';
import { CategoriasService } from 'src/app/services/categorias.service';

@Component({
  selector: 'app-categoria-new-dialog',
  templateUrl: './categoria-new-dialog.component.html',
  styleUrls: ['./categoria-new-dialog.component.scss']
})
export class CategoriaNewDialogComponent implements OnInit {

  categoriaDto: CategoriaDto;

  constructor(public dialogRef: MatDialogRef<CategoriaNewDialogComponent>,
    private categoriasService: CategoriasService) { }

  ngOnInit() {
    this.categoriaDto = new CategoriaDto('');
  }

  cerrar() {
    this.dialogRef.close(null);
  }

  guardarCategoria() {
    this.categoriasService.createCategoria(this.categoriaDto).then(respCorrecta => {
      this.dialogRef.close(true);
    }).catch(respError => {
      this.dialogRef.close(false);
    });
  }


}
