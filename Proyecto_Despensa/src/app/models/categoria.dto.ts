export class CategoriaDto {
    constructor(
        public nombre: string
    ) {}

    transformarDto() {
        return { 
            nombre: this.nombre
        };
    }
}