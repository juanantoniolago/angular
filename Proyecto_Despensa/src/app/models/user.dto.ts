import { Stock } from './stock.interface';
import { StockDto } from './stock.dto';

export class UserDto {

    constructor(
        public nombre: string,
        public email: string,
        public photo: string,
        public stockId:string
        
    ) {}

    transformarDto() {
        return { 
            nombre: this.nombre, 
            email: this.email,
            photo: this.photo,
            stockId: this.stockId
         
        };
    }
    
}