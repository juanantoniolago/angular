export class ProductoDto {
    constructor(
        public nombre: string,
        public descripcion: string,
        public categoria:string
        
    ) {}

    transformarDto() {
        return { 
            nombre: this.nombre, 
            descripcion: this.descripcion, 
            categoria: this.categoria
        };
    }
}