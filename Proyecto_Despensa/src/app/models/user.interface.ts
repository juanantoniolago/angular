export interface User {
    email: string;
    nombre: string;
    photo: string;
    stockId: string;
}