export interface Stock{
    producto : string;
    categoria : string;
    en_despensa : number;
    minimos : number;
}