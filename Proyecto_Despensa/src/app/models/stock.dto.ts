export class StockDto{

    constructor(
    public producto : string,
    public categoria : string,
    public en_despensa : number,
    public minimos : number){}

    transformarDto(){
        return {producto : this.producto,categoria :this.categoria,en_despensa : this.en_despensa, minimos : this.minimos}
    }
}