import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserDto } from '../models/user.dto';
import { Observable } from 'rxjs';
import { User } from '../models/user.interface';
import { StockDto } from '../models/stock.dto';
import { Stock } from '../models/stock.interface';

const collectionName = 'usuario';



@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private db: AngularFirestore) { }

  createUser(uid: string, userDto: UserDto) {
    return this.db.collection(collectionName).doc(uid).set(
      userDto.transformarDto()
    );
  }

  getUser(uid: string): Observable<User> {
    return this.db.collection(collectionName).doc<User>(uid).valueChanges();
  }

  modificarStock(cantidad:number, id:string){
    return  this.db.collection(collectionName).doc(localStorage.getItem('uid')).collection('Stock').doc(id).update(
      {en_despensa : cantidad}
    )
  }


  nuevoStock(stockDto: StockDto, id : string){
    return this.db.collection(collectionName).doc(localStorage.getItem('uid')).collection('Stock').doc(id).set(stockDto.transformarDto())
  }

  getStock() {
    return this.db.collection(collectionName).doc(localStorage.getItem('uid')).collection('Stock').snapshotChanges();
  }

  checkCodigoAcceso(codigoAleatorio: string) {
    return this.db.collection(collectionName, ref => ref.where('codigo_acceso', '==', codigoAleatorio)).valueChanges();
  }

  borrarStock(id : string){
    return  this.db.collection(collectionName).doc(localStorage.getItem('uid')).collection('Stock').doc(id).delete()
  }
  


}


