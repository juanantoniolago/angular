import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { ProductoDto } from '../models/productos.dto';
import { Producto } from '../models/productos.interface';

export const collectionName = 'productos';


@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private db: AngularFirestore) { }

  public createProducto(productoDto: ProductoDto): Promise<DocumentReference> {
    const productosCollection = this.db.collection<Producto>(collectionName);

    return productosCollection.add(productoDto.transformarDto());
  }

  public getProductos() {
    return this.db.collection<Producto>(collectionName).snapshotChanges();
  }

  public updateProducto(idProductoToUpdate: string, productoDto: ProductoDto) {
    return this.db.doc(`${collectionName}/${idProductoToUpdate}`).update(productoDto.transformarDto());
  }

  public deleteProductos(idProductosDelete: string) {
    return this.db.collection(collectionName).doc(idProductosDelete).delete();
  }

  public updateProductos(idCategoriasUpdate: string, productoDto: ProductoDto){
    return this.db.doc(`${collectionName}/${idCategoriasUpdate}`).update(productoDto);
  }
}

