import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { CategoriaDto } from '../models/categoria.dto';
import { Categoria } from '../models/categoria.interface';

export const collectionName = 'categorias';


@Injectable({
  providedIn: 'root'
})
export class CategoriasService {

  constructor(private db: AngularFirestore) { }


  public createCategoria(categoriaDto: CategoriaDto): Promise<DocumentReference> {
    // Obtengo la colección de stories
    const usuariosCollection = this.db.collection<Categoria>(collectionName);

    // Sobre la colección que he obtenido, añado el nuevo usuario
    return usuariosCollection.add(categoriaDto.transformarDto());
  }

  // [Read] listado de stories
  public getCategorias() {
    // 5. Cambio valueChanges() por snapshotChanges()
    return this.db.collection<Categoria>(collectionName).snapshotChanges();
  }

  // [Update] de story
  public updateCategoria(idCategoriaToUpdate: string, categoriaDto: CategoriaDto) {
    // 5. Cojo la opción 2 pro del deleteStory
    return this.db.doc(`${collectionName}/${idCategoriaToUpdate}`).update(categoriaDto.transformarDto());
  }

  public deleteCategorias(idCategoriasDelete: string) {
    return this.db.collection(collectionName).doc(idCategoriasDelete).delete();
  }

  public updateCategorias(idCategoriasUpdate: string, categoriasDto: CategoriaDto){
    return this.db.doc(`${collectionName}/${idCategoriasUpdate}`).update(categoriasDto);
  }
}
