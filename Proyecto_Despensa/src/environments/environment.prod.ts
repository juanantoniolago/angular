export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyAjzsJaDu5F7DbyXSa099FV_s1EJPY1Vic",
    authDomain: "proyectodespensa-60037.firebaseapp.com",
    databaseURL: "https://proyectodespensa-60037.firebaseio.com",
    projectId: "proyectodespensa-60037",
    storageBucket: "proyectodespensa-60037.appspot.com",
    messagingSenderId: "661214696545",
    appId: "1:661214696545:web:5d1c0b1448eb20a6e74620",
    measurementId: "G-VJC89BQJ6C"
  },
  baseUrl: 'http://www.miguelcamposrivera.com/api'
};
