// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAjzsJaDu5F7DbyXSa099FV_s1EJPY1Vic",
    authDomain: "proyectodespensa-60037.firebaseapp.com",
    databaseURL: "https://proyectodespensa-60037.firebaseio.com",
    projectId: "proyectodespensa-60037",
    storageBucket: "proyectodespensa-60037.appspot.com",
    messagingSenderId: "661214696545",
    appId: "1:661214696545:web:5d1c0b1448eb20a6e74620",
    measurementId: "G-VJC89BQJ6C"
  },
  baseUrl: 'http://localhost:9000/api'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
