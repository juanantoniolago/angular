import { Component, OnInit } from '@angular/core';
import { Stories } from 'src/app/models/stories.interface';
import { StoriesService } from 'src/app/services/stories.service';
import { StoriesNewDialogComponent } from '../stories-new-dialog/stories-new-dialog.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FirestoreResponse } from 'src/app/models/firestore-response.interface';

@Component({
  selector: 'app-stories-listado',
  templateUrl: './stories-listado.component.html',
  styleUrls: ['./stories-listado.component.scss']
})
export class StoriesListadoComponent implements OnInit {
  listadoStories: FirestoreResponse<Stories>[];
  displayedColumns: string[] = ['titulo','estado','acciones']


  constructor(
    private storiesService: StoriesService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
    ) { }

  ngOnInit() {
    this.loadStories();
  }

  loadStories(){
    this.storiesService.getStories().subscribe(resp=> {
      this.listadoStories= [];
      
      resp.forEach((story:any) =>{
        this.listadoStories.push({
          id: story.payload.doc.id,
          data: story.payload.doc.data()
         });
      

      });
    })
  }

  dialogNewStorie() {
    let dialogRef = this.dialog.open(StoriesNewDialogComponent, {
      width: '300px'
    });

    dialogRef.afterClosed().subscribe(resp => {
      if(resp != null) {
        if(resp) {
          this.snackBar.open("Storie creada correctamente", );
        } else {
          this.snackBar.open("Error al crear storie");
        }
      }
    });
  }

  dialogEditStorie(storieToEdit: Stories) {
    let dialogRef = this.dialog.open(StoriesNewDialogComponent, {
      width: '300px',
      data: { story: storieToEdit}
  });
  }


  deleteStory(idStorieToDelete: string) {

  }



}
