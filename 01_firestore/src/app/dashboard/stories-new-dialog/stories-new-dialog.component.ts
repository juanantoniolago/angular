import { Component, OnInit } from '@angular/core';
import { StoriesService } from 'src/app/services/stories.service';
import { MatDialogRef } from '@angular/material';
import { StoriesDto } from 'src/app/models/stories.dto';

@Component({
  selector: 'app-stories-new-dialog',
  templateUrl: './stories-new-dialog.component.html',
  styleUrls: ['./stories-new-dialog.component.scss']
})
export class StoriesNewDialogComponent implements OnInit {
  storiesDto: StoriesDto;

  constructor(
    public dialogRef: MatDialogRef<StoriesNewDialogComponent>,
    private storiesService: StoriesService
    ) { }

  ngOnInit() {
    this.storiesDto = new StoriesDto('','','');

  }

  guardarStorie() {
    this.storiesService.createStorie(this.storiesDto).then(respCorrecta => {
      this.dialogRef.close(true);
    }).catch(respError => {
      this.dialogRef.close(false);
    });
  }

  cerrar() {
    this.dialogRef.close(null);
  }

}
