import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { StoriesListadoComponent } from './stories-listado/stories-listado.component';

export const DashboardRoutes: Routes = [
  {path: 'inicio', component: DashboardComponent},
  { path: 'stories', component: StoriesListadoComponent },
  { path: '', redirectTo: '/session/signin', pathMatch: 'full' }
  
];
