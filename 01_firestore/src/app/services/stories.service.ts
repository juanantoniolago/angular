import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Stories } from '../models/stories.interface';
import { StoriesDto } from '../models/stories.dto';

export const collectionName = 'stories';


@Injectable({
  providedIn: 'root'
})
export class StoriesService {

  constructor(private db: AngularFirestore) { }

  // CRUD (Create Read Update Delete)
  // [Create] un usuario
  // [Create] un usuario
  public createStorie(storiesDto: StoriesDto): Promise<DocumentReference> {
    // Obtengo la colección de usuarios
    const usuariosCollection = this.db.collection<Stories>(collectionName);

    // Sobre la colección que he obtenido, añado el nuevo usuario
    return usuariosCollection.add(storiesDto.transformarDto());
  }

  // [Read] listado de stories
  public getStories() {
    return this.db.collection<Stories>(collectionName).snapshotChanges();
  }

  // [Update] de stories
  public updateStories() {}

  // [Delete] de stories
  public deleteStories() {}
}



