export class UserDto{
    constructor(
        public nombre: string,
        public email: string,
        public photo: string
    ){}

    transformarDto() {
        return { 
            titulo: this.nombre, 
            email: this.email, 
            estado: this.photo
        };
    }
}