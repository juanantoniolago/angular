// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD0ooobuZIWfK0554bsCxW4TLk4R_QvbF0",
    authDomain: "proyectodam-a2ea6.firebaseapp.com",
    databaseURL: "https://proyectodam-a2ea6.firebaseio.com",
    projectId: "proyectodam-a2ea6",
    storageBucket: "proyectodam-a2ea6.appspot.com",
    messagingSenderId: "1027987904195",
    appId: "1:1027987904195:web:1f846ab242a679579e7476",
    measurementId: "G-LZ767D0BKR"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
