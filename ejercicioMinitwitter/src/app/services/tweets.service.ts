import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TweetResponse } from '../models/tweet-response';
import { Tweet } from '../models/tweet';



const tweetsURL = 'https://www.minitwitter.com:3001/apiv1/tweets/all';
const apiKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjo2MzUsInVzZXJuYW1lIjoianVhbml4aTk3IiwiZW1haWwiOiJqdWFuLmFudG9uaW8ubGFnby45N0BnbWFpbC5jb20iLCJwaG90b1VybCI6IiIsImFjdGl2ZSI6ZmFsc2UsImNyZWF0ZWQiOiIyMDE5LTEwLTI4VDA4OjQyOjA5LjAwMFoifSwiZXhwIjoxNTcyODU2OTgwLCJpYXQiOjE1NzIyNTIxNzl9.hSkuBpHDAxxMQ-FxvQ2isowjFpA4fLiKhmwHYxs3hOY';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    })
  };

@Injectable({
  providedIn: 'root'
})

export class TweetsService {

  constructor(private http: HttpClient) {
  }

 
  public getTweet(idPelicula: string): Observable<Tweet> {
    return this.http.get<Tweet>(
      tweetsURL + '/popular?api_key=' + apiKey,
    );
  }

  public getTweets(): Observable<TweetResponse> {
    return this.http.get<TweetResponse>(
      tweetsURL + '/popular?api_key=' + apiKey,
    );
  }
  
  
 
}