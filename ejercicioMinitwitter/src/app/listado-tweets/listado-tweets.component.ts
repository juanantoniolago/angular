import { Component, OnInit } from '@angular/core';
import { TweetsService } from '../services/tweets.service';
import { Tweet} from '../models/tweet'


@Component({
  selector: 'app-listado-tweets',
  templateUrl: './listado-tweets.component.html',
  styleUrls: ['./listado-tweets.component.css']
})
export class ListadoTweetsComponent implements OnInit {

  tweets: Tweet[];
  constructor(private tweetsService: TweetsService) { }

  ngOnInit() {
    
  }

 

}
