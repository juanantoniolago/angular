import { Tweet } from './tweet';

export interface TweetResponse {
    count: number;
    next: string;
    previous?: any;
    results: Tweet[];
}