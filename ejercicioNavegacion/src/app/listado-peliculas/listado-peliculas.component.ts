import { Component, OnInit } from '@angular/core';
import { Pelicula } from '../models/pelicula.interface';

@Component({
  selector: 'app-listado-peliculas',
  templateUrl: './listado-peliculas.component.html',
  styleUrls: ['./listado-peliculas.component.css']
})
export class ListadoPeliculasComponent implements OnInit {
  listadoPeliculas: Pelicula[];
  columsToDisplay = ['poster_path','adult','release_date']

  constructor() { }

  ngOnInit() {
  
  }

}
