import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PeliculaResponse } from '../models/peliculaResponse.interface';

const authURL = 'https://api.themoviedb.org/3/movie/550?api_key=99c4f505f9b9ab5a4e331f455f14bb4d';

const requestOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  @Injectable({
    providedIn: 'root'
  })
  export class PeliculaService {
  
    constructor(private http: HttpClient) {  
    }

    public getPelicula(): Observable<PeliculaResponse> {
        return this.http.get<PeliculaResponse>(
          authURL,
          requestOptions
        );
      }
  }
