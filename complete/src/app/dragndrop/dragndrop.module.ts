import {
  MatCardModule,
  MatCheckboxModule,
  MatIconModule,
  MatListModule
} from '@angular/material';

import { CommonModule } from '@angular/common';
import { DragndropComponent } from './dragndrop.component';
import { DragndropRoutes } from './dragndrop.routing';
import { DragulaModule } from 'ng2-dragula';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DragndropRoutes),
    MatIconModule,
    MatCardModule,
    MatCheckboxModule,
    MatListModule,
    FlexLayoutModule,
    DragulaModule.forRoot()
  ],
  declarations: [DragndropComponent]
})
export class DragndropModule {}
